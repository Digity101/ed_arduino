// Pins for vibration motor
int vibrationPin = 12;

void setup(){
  //Setup pins used for vibration motor
  pinMode(vibrationPin, OUTPUT);
}

void loop(){
  vibrate(vibrationPin, 1000, 255); //vibrate for 1s at 100% pwm
}

// pin is pin used for vibration motor
// duration of vibration in ms
// pwm is 0=0% and 255=100%
void vibrate(int pin, int duration, int pwm){
  analogWrite( pin , pwm );
  delay(duration);
  analogWrite( pin , 0 ); //off  
}
