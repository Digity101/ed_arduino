#include <MadgwickAHRS.h>
#include "BMI088.h"
SoftwareI2C WireS1;
BMI088 bmi088( BMI088_ACC_ADDRESS, BMI088_GYRO_ADDRESS );

float pitch, roll, heading;
float gx, gy, gz, ax, ay, az;

Madgwick filter; //Filter to combine gyro + accel to get rotation
unsigned long microsPerReading, microsPrevious;

void setup(void) {
    bmi088.initSoftwareI2C(&WireS1, 10, 9);  //initSoftwareI2C, sda=2, scl=3 on flora
    //bmi088.initSoftwareI2C(&WireS1, 2, 3);
    Serial.begin(112500);

    while (!Serial);
    Serial.println("BMI088 Raw Data");

    while (1) {
        if (bmi088.isConnection()) {
            bmi088.initialize();
            bmi088.setGyroOutputDataRate(ODR_100_BW_32);

            filter.begin(100);
            Serial.println("BMI088 is connected");

            // initialize variables to pace updates to correct rate
            microsPerReading = 1000000 / 100;
            microsPrevious = micros();
  
            break;
        } else {
            Serial.println("BMI088 is not connected");
        }
        delay(2000);
    }
}

void loop(void) {

  unsigned long microsNow;
  // check if it's time to read data and update the filter
  microsNow = micros();
  if (microsNow - microsPrevious >= microsPerReading) {

    // read from sensor
    bmi088.getAcceleration(&ax, &ay, &az);
    bmi088.getGyroscope(&gx, &gy, &gz);

    // update the filter, which computes orientation
    filter.updateIMU(gx, gy, gz, ax, ay, az);

    // print the heading, pitch and roll
    roll = filter.getRoll();
    pitch = filter.getPitch();
    heading = filter.getYaw();
    
    Serial.println("heading, pitch, roll");
    Serial.print(heading);
    Serial.print(" ");
    Serial.print(pitch);
    Serial.print(" ");
    Serial.println(roll);

    /*
     * https://learn.adafruit.com/adafruit-bno055-absolute-orientation-sensor/webserial-visualizer
     * Visit  chrome://flags from within Chrome. Find and enable the Experimental Web Platform features
     * Verify 112500 Baud 
     * Go to https://adafruit-3dmodel-viewer.glitch.me/
     * Set Quaternatiatons to Euler angles
     * Rotate bunny
     * 
    Serial.print(F("Orientation: "));
    Serial.print(roll);
    Serial.print(F(", "));
    Serial.print(pitch);
    Serial.print(F(", "));
    Serial.print(heading);
    Serial.println(F(""));
     * 
     * 
     */

    // increment previous time, so we keep proper pace
    microsPrevious = microsPrevious + microsPerReading;
  }
}
