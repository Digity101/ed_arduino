#include "BMI088.h"

#include "SoftwareI2C.h"  
SoftwareI2C WireS1;
SoftwareI2C WireS2;

// Pins for vibration motor
int vibrationLowPin = 1;
int vibrationUpPin = 12;

//initial variables for acccelrometer b-prefix is bottom, t-prefix is top
float lax = 0, lay = 0, laz = 0;
float lgx = 0, lgy = 0, lgz = 0;

float uax = 0, uay = 0, uaz = 0;
float ugx = 0, ugy = 0, ugz = 0;

BMI088 bmi088Low(BMI088_ACC_ADDRESS, BMI088_GYRO_ADDRESS);
BMI088 bmi088Up(BMI088_ACC_ALT_ADDRESS, BMI088_GYRO_ALT_ADDRESS);

void setup(){
  //Setup pins used for vibration motor
  pinMode(vibrationLowPin, OUTPUT);
  pinMode(vibrationUpPin, OUTPUT);
  
  bmi088Low.initSoftwareI2C(&WireS1, 2, 3);  //initSoftwareI2C, sda=2, scl=3 on flora
  bmi088Low.initialize();

  bmi088Up.initSoftwareI2C(&WireS2, 10, 9);
  bmi088Up.initialize();

  Serial.begin(115200);  
}

void loop(){
  
  //Read data from sensors
  bmi088Low.getAcceleration(&lax, &lay, &laz);
  bmi088Low.getGyroscope(&lgx, &lgy, &lgz);
  
  bmi088Up.getAcceleration(&uax, &uay, &uaz);
  bmi088Up.getGyroscope(&ugx, &ugy, &ugz);
  
  printLowerSensor();
  printUperSensor();


  vibrate(vibrationLowPin, 1000, 255); //vibrate for 1s at 100% PWM
  vibrate(vibrationUpPin, 1000, 255); //vibrate for 1s at 100% PWM
}

// pin is pin used for vibration motor
// duration of vibration in ms
// pwm is 0=0% and 255=100%
void vibrate(int pin, int duration, int pwm){
  analogWrite( pin , pwm );
  delay(duration);
  analogWrite( pin , 0 ); //off  
}

void printLowerSensor(){
  Serial.println((String)"lax:"+lax+" lay:"+lay+" laz:"+laz+" lgx:"+lgx+" lgy:"+lgy+" lgz:"+lgz);
  delay(50);
}

void printUperSensor(){
  Serial.println((String)"uax:"+uax+" uay:"+uay+" uaz:"+uaz+" ugx:"+ugx+" ugy:"+ugy+" ugz:"+ugz);
  delay(50);
}
