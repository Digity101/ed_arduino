#include <RunningMedian.h>
#include <BMI088.h>
#include <SoftwareI2C.h>

#define DurationReadings 5 //Taking the median over 60s
#define NumberOfReadings 30 // With 30 readings

#define vibrationDuration 1000 //in miliseconds

//Wires for I2C
SoftwareI2C WireS1;
SoftwareI2C WireS2;

// Pins for vibration motor
int vibrationLowPin = 12;
int vibrationUpPin = 1;

//initial variables for acccelrometer b-prefix is bottom, t-prefix is top
float lax, lay, laz, lgx, lgy, lgz;
float uax, uay, uaz, ugx, ugy, ugz;

RunningMedian layMedian = RunningMedian(NumberOfReadings);
RunningMedian uayMedian = RunningMedian(NumberOfReadings);

BMI088 bmi088Low(BMI088_ACC_ADDRESS, BMI088_GYRO_ADDRESS);
BMI088 bmi088Up(BMI088_ACC_ADDRESS, BMI088_GYRO_ADDRESS);

unsigned long milisPerReading, milisPrev; //For timing between saving data in arrays to calculate median over time.

//Variables for vibrating without delay
unsigned long milisUpperVibStart, milisLowerVibStart;
bool upperVibrating, lowerVibrating;

void setup() {
  //Setup pins used for vibration motor
  pinMode(vibrationLowPin, OUTPUT);
  pinMode(vibrationUpPin, OUTPUT);

  bmi088Up.initSoftwareI2C( & WireS1, 2, 3); //initSoftwareI2C, sda=2, scl=3 on flora
  setupSensor( &bmi088Up, "Upper sensor");

  bmi088Low.initSoftwareI2C( & WireS2, 10, 9);
  setupSensor( &bmi088Low, "Lower sensor");

  Serial.begin(115200);

  // initialize variables to pace updates to correct rate
  milisPerReading = (long) DurationReadings * 1000 / NumberOfReadings;
  milisPrev = millis();
}

void setupSensor(BMI088 * sensor, const char * name) {
  while (1) {
    if (sensor -> isConnection()) {
      sensor -> initialize();
      //sensor->setAccScaleRange(RANGE_3G );
      //sensor->setGyroScaleRange(RANGE_125);
      sensor -> setGyroOutputDataRate(ODR_100_BW_32);

      Serial.print(name);
      Serial.println(" is connected");
      break;
    } else {
      Serial.print(name);
      Serial.println(" is not connected");
    }
    delay(2000);
  }
}

void loop() {

  unsigned long milisNow;

  milisNow = millis(); // check if we need to put reading into median array;
  if (milisNow - milisPrev >= milisPerReading) {
    //Read sensor data
    bmi088Low.getAcceleration( &lax, &lay, &laz);
    bmi088Up.getAcceleration( &uax, &uay, &uaz);

    //Add to running median
    layMedian.add(lay);
    uayMedian.add(uay);

    milisPrev = milisPrev + milisPerReading;
  }

  if (checkUpperback()) {
    //Upper back is in wrong position
    Serial.println("upper vibration");
    startVibration(vibrationUpPin, 255, &upperVibrating, &milisUpperVibStart);
  } else if (checkLowerback()) {
    //Lower back is in wrong position
    Serial.println("lower vibration");
    Serial.println(lowerVibrating);
    startVibration(vibrationLowPin, 255, &lowerVibrating, &milisLowerVibStart);
  }

  stopVibration(vibrationUpPin, &upperVibrating, &milisUpperVibStart, &milisNow);
  stopVibration(vibrationLowPin, &lowerVibrating, &milisLowerVibStart, &milisNow);

}

//Returns true if upperback is fucked up
bool checkUpperback() {
  Serial.print("Upper: ");
  Serial.println(uayMedian.getMedian());
  return uayMedian.getMedian() < 800 && uayMedian.getMedian() > 6.9;
}

//Returns true if lowerBAck is fucked up
bool checkLowerback() {
  Serial.print("Lower: ");
  Serial.println(layMedian.getMedian());
  return layMedian.getMedian() > -800 && layMedian.getMedian() < -6.9;
}

// pin is pin used for vibration motor
// pwm is 0=0% and 255=100%
// vibstatus, global boolean to check if the vibration motor is already vibrating
// vibrationStart, global long to set starting time
void startVibration(int pin, int pwm, bool* vibStatus, unsigned long* vibrationStart) {
  if (vibStatus != 0) {
    analogWrite(pin, pwm);
    *vibStatus = true;
    *vibrationStart = millis();
  }
}

void stopVibration(int pin, bool* vibStatus, unsigned long* vibrationStart, unsigned long* currentTime) {
  if (*currentTime > *vibrationStart + vibrationDuration) {
    analogWrite(pin, 0);
    *vibStatus = 0;
  }
}