See https://learn.adafruit.com/getting-started-with-flora?view=all#windows-setup on how to setup the arduino

The file needs several libarys.

From Arduino IDE > Sketch > Include Library > Manage Library

Add: [RunningMedian](https://github.com/RobTillaart/RunningMedian), [Madgwick](https://github.com/arduino-libraries/MadgwickAHRS) and the custom library [BMI088_OnSoftwareI2C](./BMI088_OnSoftwareI2C) which is provided in this repo.
![](./wiring.png)