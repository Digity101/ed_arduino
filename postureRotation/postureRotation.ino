#include <RunningMedian.h>
#include <BMI088.h>
#include <MadgwickAHRS.h>
#include <SoftwareI2C.h>

#define DurationReadings 5 //Taking the median over 60s
#define NumberOfReadings 30 // With 30 readings

#define vibrationDuration 1000 //in miliseconds

//Wires for I2C
SoftwareI2C WireS1;
SoftwareI2C WireS2;

// Pins for vibration motor
int vibrationLowPin = 12;
int vibrationUpPin = 1;

//initial variables for acccelrometer b-prefix is bottom, t-prefix is top
RunningMedian lrollMedian = RunningMedian(NumberOfReadings);
Madgwick lfilter;

RunningMedian urollMedian = RunningMedian(NumberOfReadings);
Madgwick ufilter;

BMI088 bmi088Low(BMI088_ACC_ADDRESS, BMI088_GYRO_ADDRESS);
BMI088 bmi088Up(BMI088_ACC_ADDRESS, BMI088_GYRO_ADDRESS);

unsigned long microsPerReading, microsPrevious; //For timing between reading sensor data
unsigned long milisPerReading, milisPrev; //For timing between saving data in arrays to calculate median over time.
unsigned long milisInit;

//Variables for vibrating without delay
unsigned long milisUpperVibStart, milisLowerVibStart;
bool upperVibrating, lowerVibrating;

void setup() {
  //Setup pins used for vibration motor
  pinMode(vibrationLowPin, OUTPUT);
  pinMode(vibrationUpPin, OUTPUT);

  bmi088Up.initSoftwareI2C( &WireS1, 2, 3); //initSoftwareI2C, sda=2, scl=3 on flora
  setupSensor( &bmi088Up, &lfilter, "Upper sensor");

  bmi088Low.initSoftwareI2C( & WireS2, 10, 9);
  setupSensor( &bmi088Low, &ufilter, "Lower sensor");

  Serial.begin(115200);

  // initialize variables to pace updates to correct rate
  microsPerReading = 1000000 / 100; //(10 millisec)
  microsPrevious = micros();
  milisInit = millis();

  milisPerReading = (long) DurationReadings * 1000 / NumberOfReadings;
  milisPrev = millis();
}

void setupSensor(BMI088* sensor, Madgwick* filter, const char* name) {
  while (1) {
    if (sensor -> isConnection()) {
      sensor -> initialize();
      //sensor->setAccScaleRange(RANGE_3G );
      //sensor->setGyroScaleRange(RANGE_125);
      sensor -> setGyroOutputDataRate(ODR_100_BW_32);

      filter -> begin(100);
      Serial.print(name);
      Serial.println(" is connected");
      break;
    } else {
      Serial.print(name);
      Serial.println(" is not connected");
    }
    delay(2000);
  }
}

void loop() {

  unsigned long microsNow;
  unsigned long milisNow;
  // check if it's time to read data and update the filter
  microsNow = micros();
  if (microsNow - microsPrevious >= microsPerReading) {
    readSensor( &bmi088Low, &lfilter);
    readSensor( &bmi088Up, &ufilter);

    microsPrevious = microsPrevious + microsPerReading;
  }

  milisNow = millis(); // check if we need to put reading into median array;
  if (milisNow - milisPrev >= milisPerReading) {
    lrollMedian.add(lfilter.getRoll());
    urollMedian.add(ufilter.getRoll());
    milisPrev = milisPrev + milisPerReading;
  }

  if (milisNow > milisInit + 20000) { //20s headstart
    if (checkUpperback()) {
      //Upper back is in wrong position
      Serial.println("upper vibration");
      startVibration(vibrationUpPin, 255, &upperVibrating, &milisUpperVibStart);
    } else if (checkLowerback()) {
      //Lower back is in wrong position
      Serial.println("lower vibration");
      startVibration(vibrationLowPin, 255, &lowerVibrating, &milisLowerVibStart);
    }
    stopVibration(vibrationUpPin, &upperVibrating, &milisUpperVibStart, &milisNow);
    stopVibration(vibrationLowPin, &lowerVibrating, &milisLowerVibStart, &milisNow);
  }
}

void readSensor(BMI088* sensor, Madgwick* filter) {
  float ax, ay, az, gx, gy, gz;

  // read from sensor
  sensor -> getAcceleration(&ax, &ay, &az);
  sensor -> getGyroscope(&gx, &gy, &gz);

  //Idk if we need to feed runningMedian to the Madgwick algorithm
  //update the filter, which computes orientation
  filter -> updateIMU(gx, gy, gz, ax, ay, az);

  /* // print the heading, pitch and roll  
    Serial.println("yaw, pitch, roll");
    Serial.print(filter->getYaw());
    Serial.print(" ");
    Serial.print(filter->getPitch());
    Serial.print(" ");
    Serial.println(filter->getRoll());*/
}

//Returns true if upperback is fucked up
bool checkUpperback() {
  Serial.print("Upper: ");
  Serial.println(urollMedian.getMedian());
  return urollMedian.getMedian() < 80 && urollMedian.getMedian() > 10;
}

//Returns true if lowerBAck is fucked up
bool checkLowerback() {
  Serial.print("Lower: ");
  Serial.println(lrollMedian.getMedian());
  return lrollMedian.getMedian() < -100 && lrollMedian.getMedian() > -150;
}

// pin is pin used for vibration motor
// pwm is 0=0% and 255=100%
// vibstatus, global boolean to check if the vibration motor is already vibrating
// vibrationStart, global long to set starting time
void startVibration(int pin, int pwm, bool * vibStatus, unsigned long * vibrationStart) {
  if (vibStatus) {
    analogWrite(pin, pwm);
    *vibStatus = true;
    *vibrationStart = millis();
  }
}

void stopVibration(int pin, bool * vibStatus, unsigned long * vibrationStart, unsigned long * currentTime) {
  if (*currentTime > *vibrationStart + vibrationDuration) {
    analogWrite(pin, 0);
    *vibStatus = false;
  }
}
