#include "BMI088.h"

#include "SoftwareI2C.h"  
SoftwareI2C WireS1;
SoftwareI2C WireS2;

//initial variables for acccelrometer b-prefix is bottom, t-prefix is top
float lax = 0, lay = 0, laz = 0;
float lgx = 0, lgy = 0, lgz = 0;

float uax = 0, uay = 0, uaz = 0;
float ugx = 0, ugy = 0, ugz = 0;


BMI088 bmi088Low( BMI088_ACC_ADDRESS, BMI088_GYRO_ADDRESS );
BMI088 bmi088Up( BMI088_ACC_ADDRESS, BMI088_GYRO_ADDRESS );



void setup(void) {

    bmi088Low.initSoftwareI2C(&WireS1, 2, 3);  //initSoftwareI2C, sda=2, scl=3 on flora
    bmi088Up.initSoftwareI2C(&WireS2, 10, 9);

    Serial.begin(115200);

    while (!Serial);
    Serial.println("BMI088 Raw Data");
    
    //Low check
    while (1) {
        if (bmi088Low.isConnection()) {
            bmi088Low.initialize();
            Serial.println("BMI088LOW is connected");
            break;
        } else {
            Serial.println("BMI088LOW is not connected");
        }
        delay(2000);
    }

    //Upper check
    while (1) {
        if (bmi088Up.isConnection()) {
            bmi088Up.initialize();
            Serial.println("bmi088Upper is connected");
            break;
        } else {
            Serial.println("bmi088Upper is not connected");
        }
        delay(2000);
    }
}

void loop(void) {
    bmi088Low.getAcceleration(&lax, &lay, &laz);
    bmi088Low.getGyroscope(&lgx, &lgy, &lgz);

    Serial.println((String)"lax:"+lax+" lay:"+lay+" laz:"+laz+" lgx:"+lgx+" lgy:"+lgy+" lgz:"+lgz);

    bmi088Up.getAcceleration(&uax, &uay, &uaz);
    bmi088Up.getGyroscope(&ugx, &ugy, &ugz);
    Serial.println((String)"uax:"+uax+" uay:"+uay+" uaz:"+uaz+" ugx:"+ugx+" ugy:"+ugy+" ugz:"+ugz);

    Serial.println();

    delay(50);
}
